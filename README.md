# 图书管理系统

#### 介绍
技术栈：SpringBoot、Vue、Shiro等等是一个图书管理系统并且有一个用户的管理系统。

#### 效果展示

![输入图片说明](https://images.gitee.com/uploads/images/2021/0729/201646_149aa4bf_9247124.png "9ca568f3c41f12635d38dd4e864b98b.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0729/201708_c86d00f8_9247124.png "d7821405ef10888f48498d128d32b67.png")


### 基本功能
图书页面的增删查改
管理页面根据用户权限等级显示不同的菜单页面，只有管理员用户才能看到用户管理菜单，并且进行用户的增删查改
