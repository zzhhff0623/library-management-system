package com.lingnan.myschool.pojo;

import lombok.Data;

import javax.persistence.Column;

@Data
public class Book {
                 int id;
    private Category category;
                String cover;
                String title;
                 String author;
                 String date;
                 String press;
                 String abs;
                 String cid;
}
